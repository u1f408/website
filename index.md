---
layout: page
title: hewwo, world
---

this is the internet website of **lauren**, also known as **ren**.
as a genderless cat person with a love of shiny things, ren uses
[xe/xem/xyr pronouns](https://pronoun.is/xe/xem/xyr).

ren writes a lot of code, listens to a lot of music, and finds it weird
to write about xemself in third person.

## projects

* [polymorphOS] - a toy OS in Rust
    * and related projects, like [polymorph-allocator]
* [magenta] - a simple SSO protocol, and an authentication provider
    implementing that protocol
* and some more, coming to this page soon!

[polymorphOS]: https://git.sr.ht/~ren/polymorphos
[polymorph-allocator]: https://git.sr.ht/~ren/polymorph-allocator
[magenta]: https://sr.ht/~ren/magenta

## contact

* **irc:** `lauren` on [irc.tilde.chat](https://tilde.chat)
* **email:** `lauren (at) iris (dot) ac (dot) nz`
* **xmpp:** `lauren@hmm.st`
