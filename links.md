---
title: the link tree
layout: page
permalink: /links/
---

## who are we?

we're the iris system, a collective that includes:

* 🐈 lauren (or ren) &middot; xe/xem/xyr pronouns &middot; genderfluid
* 💜 danya &middot; she/her pronouns &middot; demigirl
* 📘 blue &middot; she/her or they/them pronouns &middot; ???
* … and maybe some others?

the person who controls [this website and it's contents]({{ site.baseurl }}/)
is **ren**, the host of the system.

## our social media

* **fediverse:** [iris@cloudisland.nz](https://cloudisland.nz/@iris){:rel="me"}
* **twitter:** [@raspberrymiku](https://twitter.com/raspberrymiku){:rel="me"}
* **twitch:** [raspberryMiku](https://twitch.tv/raspberryMiku){:rel="me"}
* **discord server:**
    * it's called _meow!_ and it's a cool place to hang out
    * [come join us!](https://discord.gg/MTjPQSe)

## help us survive, maybe?

* [we have a **ko-fi** account](https://ko-fi.com/irissystem)
* [and also a **patreon** account](https://patreon.com/lauren_nyaa)

we're on welfare and it's not enough to pay the bills. if you can help,
we'd really appreciate it. 💙 💙 💙
