---
layout: post
title: polymorphOS status update - 2019-10-29
tags:
  - polymorphos
---

[NOTE: this post has been brought over from the old wordpress blog]

quick update! didn't do anything to the kernel itself today, i was mainly focusing on the allocator, but -

polymorph-allocator now has the ability to deallocate blocks properly, and it seems to work okay! i tagged v0.1.0 and [published it to crates.io](https://crates.io/crates/polymorph-allocator) so it can be used in other things. i also [went through and documented everything, which you can see on docs.rs](https://docs.rs/polymorph-allocator/0.1.0).
