---
layout: post
title: polymorphOS status update - 2019-10-28
tags:
  - polymorphos
---

[NOTE: this post has been brought over from the old wordpress blog]

today has been the day of the ~memory manager~ !!

previously, polymorphOS has been using [the `linked_list_allocator` crate](https://crates.io/crates/linked_list_allocator). there's a few downsides to that crate, one of which is you can't add non-contiguous memory blocks.

i've made memory managers before (see [awooos/dmm](https://github.com/awooos/dmm)) but those were in C so i couldn't just grab one and run with it. so, i wrote a new allocator named polymorph-allocator, that takes heavy inspiration from awooos/dmm.

it's very early in development - it can allocate memory but it doesn't do proper deallocation (yet) - but polymorphOS boots and works using the new allocator! i'm very happy with this so far.

currently the polymorphOS kernel only tells the memory manager about one memory block (which is the heap that was created for `linked_list_allocator`), but soon it'll know about all memory in the system!
