---
layout: post
title: polymorphOS status update - 2019-10-27
tags:
  - polymorphos
---

[NOTE: this post has been brought over from the old wordpress blog]

i figured i'd start doing updates on how polymorphOS is going! so here's the first one.

i've implemented a driver system that has two load phases - "early" and "late". the "early" phase runs as early in the boot process as possible (just after control has been passed to kmain), and the "late" phase runs after the PCI bus so it can set up drivers for installed PCI devices.

i've also ripped out the uart\_16550 crate and replaced it with my own serial port driver, loaded in the "early" phase, that accepts data to write to the serial port through the kernel event system. in order to get klog and friends to write to the serial port, i've added a stub `impl Write` struct that just sends events to the serial port driver (with the `immediate` flag set).

![](https://lauren-dump.sfo2.digitaloceanspaces.com/blog/1572226826-vmware-6dd28408.png)

in this screenshot, the VGA output is on the left, and the serial port output is in the terminal on the right. you can see a few messages on the left (before the `End of drivers::setup_early()` message) that aren't outputted to the serial port - these are logged before the serial port driver is initialized.

next up on the to do list:

* timers with callbacks (think `setTimeout()` in JS)
* implementing read support in my serial port driver
* implementing a very tiny readline-esque thing for …
* … a debugging shell over the serial port!
* writing a new memory manager so i can use all the available memory
