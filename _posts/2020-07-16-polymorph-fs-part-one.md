---
layout: post
title: polymorph-fs - trait-based in-kernel filesystem handling for polymorphOS
tags:
  - polymorphos
---

As part of getting driver handling into polymorphOS,
I wanted to be able to load drivers
from an in-memory filesystem,
given to the OS
by the bootloader as an initrd.

I wanted the implementation of this
to be able to have
what are effectively virtual file nodes,
where writing to the virtual file
executes what has been written
as meow-forth code inside
a polymorphOS driver interpreter context.

My initial thoughts on how to get that to work
revolved around Rust traits -
have a struct acting as a container
for the kernel driver meow-forth instances
that implements a trait that the filesystem handling
expects, which presents each interpreter
as a directory, much like the 
`/proc` filesystem on a Linux system.

## The filesystem traits

Currently, there are two traits -
`FSProvider` and `FSFile`,
and a management struct
`Filesystem`.

Anything that wants to present
a directory tree within a filesystem
can implement the
`FSProvider`
trait, which allows for
listing files within the tree,
responding to "is this a file" or
"is this a directory" queries,
and the
`open`
method, which returns a
`Box<dyn FSFile>`.

# Driver interface

Driver support in a dialect of Forth
(using the meow-forth interpreter)
is currently being implemented
in polymorphOS,
and the design of that driver interface
was chosen specifically so
hooking it up to
the trait-based filesystem handling
would be rather easy.

Currently, there is a
`DriverContainer`
type, which implements the
`FSProvider`
trait.
The
`FSProvider`
implementation on that
delegates to individual
`Driver`
instances, which also implement
`FSProvider`.

The structure that implements
`FSFile`,
named
`DriverRefFile`,
stores the name of the driver
that it's doing file handling for,
as well as a file type value,
which determines the content
of the file.

As it currently stands,
you can mount the driver
container to a filesystem
using the
`DriverContainerFS`
helper type,
which will present
all of the loaded drivers
as directories
in the filesystem.

The core of that handling
is tested in the unit test at
`polymorph_core::driver::tests::driver_filesystem_name`,
and more tests will be implemented
as the filesystem handling for drivers
is expanded.
