---
layout: post
title: spookey - a simple python status site generator
---

`spookey`
is a simple Python static site generator
using Jinja2 for templating and
`mdoc(7)` (rendered via `mandoc(1)`)
for page content.

It's still very rough around the edges,
but it works well enough
to generate this website.

The code is available
[over on GitLab](https://gitlab.com/alxce/spookey).

# How it works
The core of
`spookey`
is the
`Site`
class, which scans the site directory
for posts (which are present in the
`_posts` directory), and pages (which can be anywhere).

First, it renders each post and page to HTML
from the source content based on file extension,
and persists the raw file content
of any file of an unknown type.

This pass parses the HTML of the page,
scanning for an HTML `h1`
tag to use as the title of the page -
there is no front matter on content here.

This also includes a special-case for
`mdoc(7)`
content, using the content of the first 
`.Sh`
section as the page title.

Second, it does a template pass on any files
rendered to HTML - first using the rendered
file itself as a Jinja2 template
(to allow for including expansions such as
a list of posts to happen within a page),
takes the result of that template render,
and provides that as the content to the
actual page template.

# More?
There's still a lot of development work
to be done on `spookey`,
and the code is open-source
under the MIT License,
so if you want to contribute,
feel free to pop over to
[the GitLab repository](https://gitlab.com/alxce/spookey).
